#H1 Bolster Naming Converntion Generator Chrome Extension

To test locally:
1. Run `npm start`
2. Navigate to `chrome://extensions/` in Chrome (make sure Developer Mode is switched on)
3. Click 'Load Unpacked' and select the build folder that was generated from running `npm start`
4. It should appear in your extensions section

[Current version can be found here](https://drive.google.com/drive/folders/1DB4tFCiO8nR5QY-PB1AjTMLGMFt8jnvB)