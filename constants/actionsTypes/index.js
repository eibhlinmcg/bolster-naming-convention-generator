import name from './name';
import pixel from './pixel';
import utm from './utm';

export default {
  name,
  pixel,
  utm,
};
