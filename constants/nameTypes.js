const nameTypes = {
  CAMPAIGN_NAME: 'CAMPAIGN_NAME',
  AD_SET_NAME: 'AD_SET_NAME',
  AD_NAME: 'AD_NAME',
};

export const nameTypeOptions = [
  {
    id: 0,
    name: 'Campaign Name',
    key: nameTypes.CAMPAIGN_NAME,
  }, {
    id: 1,
    name: 'Ad Set Name',
    key: nameTypes.AD_SET_NAME,
  }, {
    id: 2,
    name: 'Ad Name',
    key: nameTypes.AD_NAME,
  },
];

export default nameTypes;
