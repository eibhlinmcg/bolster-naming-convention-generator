import axios from 'axios';
import storage, { store } from '../storage';

const AGENCY_OS_API_ENDPOINT = 'https://staging.agencyos.io/api/';

const HTTP = axios.create({
  baseURL: AGENCY_OS_API_ENDPOINT,
  responseType: 'json',
});

// Request middleware.
HTTP.interceptors.request.use(async (config) => {
  const token = await storage.getValue(store.TOKEN);
  if (token) {
    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: `Bearer ${token}`,
      },
    };
  }
  return config;
},
error => Promise.reject(error),
);

// Response middleware.
HTTP.interceptors.response.use(
  response => Promise.resolve(response),
  error => Promise.reject(error),
);

export const authenticate = async (userDetails) => {
  try {
    const response = await HTTP.post('/login', userDetails);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

export const getCampaigns = async (userId) => {
  try {
    const response = await HTTP.get(`/users/${userId}/projects`);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};