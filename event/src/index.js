import { createStore, applyMiddleware } from 'redux';
import { wrapStore } from 'react-chrome-redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
import rootSaga from './sagas';
import portName from '../../constants/portName';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

wrapStore(store, { portName });
