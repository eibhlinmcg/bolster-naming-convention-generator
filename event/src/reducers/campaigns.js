import actions from '../../../constants/actionsTypes/campaigns';
import userActions from '../../../constants/actionsTypes/user';

const initialState = {
  fetching: false,
  error: false,
  campaigns: [],
  clients: [],
  showCampaignData: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_CAMPAIGNS:
      return { ...state, fetching: true };
    case actions.FETCH_CAMPAIGNS_SUCCESS:
      return {
        fetching: false,
        error: false,
        showCampaignData: true,
        campaigns: action.campaigns.data.map(({ attributes, id, relationships }) => ({
          ...attributes, id, client: relationships.parentOrganisation.id,
        })),
        clients: action.campaigns.included.organisations.map(({ attributes, id }) => (
          { ...attributes, id }
        )),
      };
    case actions.FETCH_CAMPAIGNS_ERROR:
      return {
        ...state,
        fetching: false,
        showCampaignData: false,
        error: action.error,
      };
    case userActions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
