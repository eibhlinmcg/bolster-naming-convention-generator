import { combineReducers } from 'redux';

import name from './name';
import user from './user';
import campaigns from './campaigns';
import pixel from './pixel';
import utm from './utm';

export default combineReducers({
  name,
  user,
  campaigns,
  pixel,
  utm,
});
