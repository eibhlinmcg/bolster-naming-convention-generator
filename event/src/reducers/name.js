import copy from 'copy-to-clipboard';
import actions from '../../../constants/actionsTypes/name';
import userActions from '../../../constants/actionsTypes/user';
import generateString from '../../../utils/generateString';

const initialState = {
  activeNameType: null,
  generatedString: null,
  inputValues: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_NAME_TYPE:
      return { ...initialState, activeNameType: action.nameType };
    case actions.GENERATE_STRING:
      const generatedString = generateString(state.activeNameType, action.inputValues);
      copy(generatedString);
      return {
        ...state,
        inputValues: action.inputValues,
        generatedString,
      };
    case actions.RESET_STATE:
    case userActions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
