import copy from 'copy-to-clipboard';
import actions from '../../../constants/actionsTypes/pixel';
import userActions from '../../../constants/actionsTypes/user';
import generatePixel from '../../../utils/generatePixel';

const initialState = {
  generatedString: null,
  inputValues: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.GENERATE_STRING:
      const generatedString = generatePixel(action.inputValues);
      copy(generatedString);
      return {
        inputValues: action.inputValues,
        generatedString,
      };
    case actions.RESET_STATE:
    case userActions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
