import actions from '../../../constants/actionsTypes/user';
import storage, { store } from '../storage';

const initialState = {
  authenticating: false,
  authenticationError: false,
  activeGenerator: 'name',
  token: null,
  id: null,
  firstName: '',
  lastName: '',
  avatarUrl: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case '@@redux/INIT':
      return {
        ...initialState,
        token: async () => await storage.getValue(store.TOKEN),
        id: async () => await storage.getValue(store.ID),
        firstName: async () => await storage.getValue(store.FIRST_NAME),
        lastName: async () => await storage.getValue(store.LAST_NAME),
        avatarUrl: async () => await storage.getValue(store.AVATAR_URL),
      };
    case actions.LOGIN:
      return { ...initialState, authenticating: true, authenticationError: false };
    case actions.LOGIN_SUCCESS:
      storage.setValue(store.TOKEN, action.token);
      storage.setValue(store.ID, action.user.id);
      storage.setValue(store.FIRST_NAME, action.user.attributes.first_name);
      storage.setValue(store.LAST_NAME, action.user.attributes.last_name);
      storage.setValue(store.AVATAR_URL, action.user.attributes.avatar_url);
      return {
        ...state,
        isLoggedIn: true,
        token: action.token,
        id: action.user.id,
        firstName: action.user.attributes.first_name,
        lastName: action.user.attributes.last_name,
        avatarUrl: action.user.attributes.avatar_url,
        authenticating: false,
        authenticationError: false,
      };
    case actions.LOGIN_ERROR:
      return {
        ...state,
        authenticating: false,
        authenticationError: action.error,
      };
    case actions.LOGOUT:
      storage.setValue(store.TOKEN, null);
      storage.setValue(store.FIRST_NAME, null);
      storage.setValue(store.LAST_NAME, null);
      storage.setValue(store.AVATAR_URL, null);
      return initialState;
    case actions.CHANGE_ACTIVE_TAB:
      return {
        ...state,
        activeGenerator: action.activeGenerator,
      };
    default:
      return state;
  }
};
