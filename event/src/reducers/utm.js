import copy from 'copy-to-clipboard';
import actions from '../../../constants/actionsTypes/utm';
import userActions from '../../../constants/actionsTypes/user';
import generateUtm from '../../../utils/generateUtm';

const initialState = {
  generatedString: null,
  inputValues: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.GENERATE_STRING:
      const generatedString = generateUtm(action.inputValues);
      copy(generatedString);
      return {
        ...state,
        inputValues: action.inputValues,
        generatedString,
      };
    case actions.RESET_STATE:
    case userActions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
