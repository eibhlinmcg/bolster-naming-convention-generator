import {
  all, takeLatest, put, call,
} from 'redux-saga/effects';
import actions from '../../../constants/actionsTypes/campaigns';
import { getCampaigns } from '../api';

function* GETCampaigns({ userId }) {
  try {
    const campaigns = yield call(getCampaigns, userId);
    yield put({ type: actions.FETCH_CAMPAIGNS_SUCCESS, campaigns });
  } catch ({ message }) {
    yield put({ type: actions.FETCH_CAMPAIGNS_ERROR, error: message });
  }
}

function* fetchCampaigns() {
  yield takeLatest(actions.FETCH_CAMPAIGNS, GETCampaigns);
}

export default function* campaignsSagas() {
  yield all([fetchCampaigns()]);
}
