import { all } from 'redux-saga/effects';
import userSagas from './user';
import campaignsSagas from './campaigns';

export default function* rootSaga() {
  yield all([userSagas(), campaignsSagas()]);
}
