import {
  all, takeLatest, put, call,
} from 'redux-saga/effects';
import actions from '../../../constants/actionsTypes/user';
import { authenticate } from '../api';

function* GETUser({ userDetails }) {
  try {
    const response = yield call(authenticate, userDetails);
    yield put({
      type: actions.LOGIN_SUCCESS,
      user: response.user,
      token: response.token,
    });
  } catch ({ error }) {
    yield put({ type: actions.LOGIN_ERROR, error });
  }
}

function* fetchUser() {
  yield takeLatest(actions.LOGIN, GETUser);
}

export default function* userSagas() {
  yield all([fetchUser()]);
}
