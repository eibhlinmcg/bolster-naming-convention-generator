export const store = {
  TOKEN: 'token',
  ID: 'id',
  FIRST_NAME: 'firstName',
  LAST_NAME: 'lastName',
  AVATAR_URL: 'avatarUrl',
};

const setValue = (key, value) => {
  chrome.storage.sync.set({ [key]: value });
};

const getValue = async (key) => {
  return await new Promise(resolve => {
    chrome.storage.sync.get(key, (item) => resolve(item[key]));
  })
  .then(value => value)
  .catch(error => error);
};

export default { setValue, getValue };