import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout } from 'antd';
import AOSLoginForm from '../components/AOSLoginForm';
import UserProfile from '../components/UserProfile';
import actions from '../../../../constants/actionsTypes/user';

const { Header, Content } = Layout;

const App = ({
  token,
  login,
  logout,
  firstName,
  lastName,
  avatarUrl,
  authenticating,
  authenticationError,
}) => (
  <Layout
    style={{
      width: '100%',
      height: '300px',
      overflowY: 'scroll',
    }}
  >
    <Header style={{ color: '#ffffff', padding: '0 10px', textAlign: 'center' }}>
      <span>{token ? 'User details' : 'Sign in to Agency OS'}</span>
    </Header>
    <Content style={{ padding: '25px' }}>
      {token ? (
        <UserProfile
          data-test="UserProfile"
          firstName={firstName}
          lastName={lastName}
          avatarUrl={avatarUrl}
          logout={logout}
        />
      ) : (
        <AOSLoginForm
          data-test="AOSLoginForm"
          login={login}
          authenticating={authenticating}
          authenticationError={authenticationError}
        />
      )}
    </Content>
  </Layout>
);

App.defaultProps = {
  token: null,
  firstName: '',
  lastName: '',
  avatarUrl: '',
};

App.propTypes = {
  token: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  avatarUrl: PropTypes.string,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  authenticationError: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
};

const mapStateToProps = ({ user }) => user;

const mapDispatchToProps = dispatch => ({
  login: userDetails => dispatch({ type: actions.LOGIN, userDetails }),
  logout: () => dispatch({ type: actions.LOGOUT }),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
