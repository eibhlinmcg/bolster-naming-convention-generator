import React from 'react';
import PropTypes from 'prop-types';
import {
  Form, Icon, Input, Button,
} from 'antd';

const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AOSLoginForm extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { form: { validateFields } } = this.props;
    validateFields();
  }

  handleSubmit(e) {
    const { form: { validateFields }, login } = this.props;
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) login(values);
    });
  }

  render() {
    const {
      form: {
        getFieldDecorator, getFieldsError, getFieldError, isFieldTouched,
      },
      authenticating,
      authenticationError,
    } = this.props;
    const emailError = isFieldTouched('email') && getFieldError('email');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
      <Form data-test="Form" onSubmit={this.handleSubmit}>
        <FormItem
          validateStatus={emailError ? 'error' : ''}
          help={emailError || ''}
        >
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email address!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Email"
            />,
          )}
        </FormItem>
        <FormItem
          validateStatus={passwordError ? 'error' : ''}
          help={passwordError || ''}
        >
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </FormItem>
        {authenticating ? (
          <Icon type="loading" data-test="LoadingIcon" />
        ) : (
          <FormItem
            data-test="LoginButtonFormItem"
            help={<span style={{ color: 'red' }}>{authenticationError || ''}</span>}
          >
            <Button
              type="primary"
              htmlType="submit"
              data-test="LoginButton"
              disabled={hasErrors(getFieldsError())}
            >
              Log in
            </Button>
          </FormItem>
        )}
      </Form>
    );
  }
}

AOSLoginForm.propTypes = {
  form: PropTypes.shape({
    validateFields: PropTypes.func.isRequired,
    getFieldDecorator: PropTypes.func.isRequired,
    getFieldsError: PropTypes.func.isRequired,
    getFieldError: PropTypes.func.isRequired,
    isFieldTouched: PropTypes.func.isRequired,
  }).isRequired,
  login: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  authenticationError: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
};

export default Form.create()(AOSLoginForm);
