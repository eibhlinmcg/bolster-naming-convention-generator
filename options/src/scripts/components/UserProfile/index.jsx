import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Button } from 'antd';

const renderSpace = (firstName, lastName) => firstName.length > 0 && lastName.length > 0;

const UserProfile = ({
  firstName, lastName, avatarUrl, logout,
}) => (
  <div
    style={{
      height: '100%',
      display: 'flex',
      justifyContent: 'space-between',
    }}
  >
    <div style={{ display: 'flex', alignItems: 'flex-end' }}>
      <Avatar src={avatarUrl} size="large">
        <span data-test="AvatarInitials">
          {firstName.charAt(0)}
          {renderSpace(firstName, lastName) ? ' ' : ''}
          {lastName.charAt(0)}
        </span>
      </Avatar>
      <h3 style={{ paddingLeft: '15px' }} data-test="NameHeader">
        {firstName}
        {renderSpace(firstName, lastName) ? ' ' : ''}
        {lastName}
      </h3>
    </div>
    <Button data-test="LogoutButton" onClick={logout}>Logout</Button>
  </div>
);

UserProfile.propTypes = {
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
};

export default UserProfile;
