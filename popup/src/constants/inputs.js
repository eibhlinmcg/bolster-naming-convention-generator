import nameTypes from '../../../constants/nameTypes';
import tactics from './name/tactics';
import phases from './name/phases';
import locations from './name/locations';
import targetingTypes from './name/targetingTypes';
import targetingDetails from './name/targetingDetails';
import adTypes from './name/adTypes';
import assetTypes from './name/assetTypes';
import events from './pixel/events';
import sources from './utm/sources';
import mediums from './utm/mediums';

export const name = {
  [nameTypes.CAMPAIGN_NAME]: {
    TACTIC: {
      key: 'tactic',
      label: 'Tactic',
      rules: [{ required: true, message: 'A tactic is required' }],
      placeholder: 'Choose a tactic',
      options: tactics,
    },
    PHASE: {
      key: 'phase',
      label: 'Phase',
      rules: [{ required: true, message: 'A phase is required' }],
      placeholder: 'Choose a phase',
      options: phases,
    },
  },
  [nameTypes.AD_SET_NAME]: {
    LOCATION: {
      key: 'location',
      label: 'Location',
      rules: [{ required: true, message: 'A location is required' }],
      placeholder: 'Choose a location',
      options: locations,
    },
    TARGETING_TYPE: {
      key: 'targetingType',
      label: 'Targeting type',
      rules: [{ required: true, message: 'A targeting type is required' }],
      placeholder: 'Choose a targeting type',
      options: targetingTypes,
    },
    TARGETING_DETAIL: {
      key: 'targetingDetail',
      label: 'Targeting detail',
      rules: [{ required: true, message: 'A targeting detail is required' }],
      placeholder: 'Choose a targeting detail',
      options: targetingDetails,
      hasCustom: true,
    },
  },
  [nameTypes.AD_NAME]: {
    AD_TYPE: {
      key: 'adType',
      label: 'Ad type',
      rules: [{ required: true, message: 'An ad type is required' }],
      placeholder: 'Choose an ad type',
      options: adTypes,
    },
    ASSET_TYPE: {
      key: 'assetType',
      label: 'Asset type',
      rules: [{ required: true, message: 'An asset type is required' }],
      placeholder: 'Choose an asset type',
      options: assetTypes,
    },
    CREATIVE_DETAIL: {
      key: 'creativeDetail',
      label: 'Creative detail',
      rules: [{ required: true, message: 'A creative detail is required' }],
      placeholder: 'Teaser video',
    },
  },
};

export const pixel = {
  PIXEL: {
    key: 'pixel',
    label: 'Pixel ID',
    rules: [{ required: true, message: 'A pixel id is required' }],
    placeholder: '1234321',
  },
  EVENT: {
    key: 'event',
    label: 'Event',
    rules: [{ required: true, message: 'An event is required' }],
    placeholder: 'Choose an event',
    options: events,
  },
};

export const utm = {
  URL: {
    key: 'url',
    label: 'URL',
    placeholder: 'https://lanewayfestival.com/',
    rules: [
      { required: true, message: 'A url is required' },
      { type: 'url', message: 'A valid url is required' },
    ],
  },
  MEDIUM: {
    key: 'medium',
    label: 'Medium',
    placeholder: 'Choose a medium',
    options: mediums,
    hasCustom: true,
  },
  SOURCE: {
    key: 'source',
    label: 'Source',
    placeholder: 'Choose a source',
    rules: [{ required: true, message: 'A source is required' }],
    options: sources,
    hasCustom: true,
  },
};

export default {
  name,
  pixel,
  utm,
};
