export default [{
  id: 0,
  name: 'Canvas',
}, {
  id: 1,
  name: 'Content',
}, {
  id: 2,
  name: 'Dark',
}, {
  id: 3,
  name: 'Display',
}, {
  id: 4,
  name: 'Event Response',
}, {
  id: 5,
  name: 'IG Story',
}, {
  id: 6,
  name: 'Native',
}, {
  id: 7,
  name: 'Post',
}, {
  id: 8,
  name: 'Search',
}];
