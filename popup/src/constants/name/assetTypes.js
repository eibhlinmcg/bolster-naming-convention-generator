export default [{
  id: 0,
  name: 'Ad Mat',
}, {
  id: 1,
  name: 'Food Shot',
}, {
  id: 2,
  name: 'Live',
}, {
  id: 3,
  name: 'Pack Shot',
}, {
  id: 4,
  name: 'Poster',
}, {
  id: 5,
  name: 'Press',
}, {
  id: 6,
  name: 'Vibe Shot',
}, {
  id: 7,
  name: 'Video',
}, {
  id: 8,
  name: 'Other',
}];
