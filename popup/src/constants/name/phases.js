export default [{
  id: 0,
  name: 'ANC',
  displayName: 'Announce',
}, {
  id: 1,
  name: 'OSD',
  displayName: 'On sale day',
}, {
  id: 2,
  name: 'PS',
  displayName: 'Pre sale',
}, {
  id: 3,
  name: 'STD',
  displayName: 'Save the date',
}, {
  id: 4,
  name: 'PO',
  displayName: 'Pre order',
}, {
  id: 5,
  name: 'ON',
  displayName: 'Out now',
}];
