export default [{
  id: 0,
  name: 'Awareness',
}, {
  id: 1,
  name: 'Educate',
}, {
  id: 2,
  name: 'Convert',
}, {
  id: 3,
  name: 'Advocate',
}];
