export default [{
  id: 0,
  name: 'Added To Cart',
}, {
  id: 1,
  name: 'Alcohol Interests',
}, {
  id: 2,
  name: 'Artist Broad',
}, {
  id: 3,
  name: 'Brand Fans',
}, {
  id: 4,
  name: 'Content Viewers',
}, {
  id: 5,
  name: 'Converted',
}, {
  id: 6,
  name: 'Email List',
}, {
  id: 7,
  name: 'Fashion Interests',
}, {
  id: 8,
  name: 'FB/IG Engagement',
}, {
  id: 9,
  name: 'FB Event',
}, {
  id: 10,
  name: 'Festival Interests',
}, {
  id: 11,
  name: 'Food & Bev Interests',
}, {
  id: 12,
  name: 'Friends of Fans',
}, {
  id: 13,
  name: 'Partner Fans',
}, {
  id: 14,
  name: 'Past Purchasers',
}, {
  id: 15,
  name: 'Publications',
}, {
  id: 16,
  name: 'Radio Interests',
}, {
  id: 17,
  name: 'Record Label Interests',
}, {
  id: 18,
  name: 'Similar Artists',
}, {
  id: 19,
  name: 'Site Visitors',
}, {
  id: 20,
  name: 'Triple J Interests',
}, {
  id: 21,
  name: 'Video Viewers',
}, {
  id: 22,
  name: 'Venue Interests',
}];
