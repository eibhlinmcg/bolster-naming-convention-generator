export default [{
  id: 0,
  name: 'Affinity',
}, {
  id: 1,
  name: 'Brand Keywords',
}, {
  id: 2,
  name: 'Category Keywords',
}, {
  id: 3,
  name: 'Display Keywords',
}, {
  id: 4,
  name: 'Engagement',
}, {
  id: 5,
  name: 'Fans',
}, {
  id: 6,
  name: 'Interests',
}, {
  id: 7,
  name: 'LaL',
}, {
  id: 8,
  name: 'Long Tail Keywords',
}, {
  id: 9,
  name: 'Owned Data',
}, {
  id: 10,
  name: 'Placement',
}, {
  id: 11,
  name: 'Site Visitors',
}, {
  id: 12,
  name: 'Topic',
}, {
  id: 13,
  name: '3rd Party Data',
}];
