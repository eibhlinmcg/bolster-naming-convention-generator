export default [{
  id: 0,
  name: 'Page view',
  track: 'PageView',
  initialScript: true,
}, {
  id: 1,
  name: 'View content',
  track: 'ViewContent',
  initialScript: false,
}, {
  id: 2,
  name: 'Search',
  track: 'Search',
  initialScript: false,
}, {
  id: 3,
  name: 'Add to cart',
  track: 'AddToCart',
  initialScript: false,
}, {
  id: 4,
  name: 'Add to wishlist',
  track: 'AddToWishlist',
  initialScript: false,
}, {
  id: 5,
  name: 'Initiate checkout',
  track: 'InitiateCheckout',
  initialScript: false,
}, {
  id: 6,
  name: 'Add payment info',
  track: 'AddPaymentInfo',
  initialScript: false,
}, {
  id: 7,
  name: 'Make purchase',
  track: 'Purchase',
  additionalTracking: " value: 0.00, currency: 'AUD',",
  initialScript: false,
}, {
  id: 8,
  name: 'Lead',
  track: 'Lead',
  initialScript: false,
}, {
  id: 9,
  name: 'Complete registration',
  track: 'CompleteRegistration',
  initialScript: false,
}];
