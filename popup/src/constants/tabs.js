import NamingConventionGenerator from '../scripts/components/NamingConventionGenerator';
import GeneratorFormInputs from '../scripts/components/GeneratorFormInputs';
import generators from '../../../constants/generators';

export default [{
  Component: NamingConventionGenerator,
  title: 'Naming',
  generator: generators.NAME,
  icon: 'tag',
}, {
  Component: GeneratorFormInputs,
  title: 'Pixel',
  generator: generators.PIXEL,
  icon: 'facebook',
}, {
  Component: GeneratorFormInputs,
  title: 'UTM',
  generator: generators.UTM,
  icon: 'global',
}];
