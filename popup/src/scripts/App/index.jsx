import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout } from 'antd';
// import ErrorManager from '../components/ErrorManager';
import ContentGeneratorForm from '../components/ContentGeneratorForm';
import actions from '../../../../constants/actionsTypes/campaigns';

const { Header, Content } = Layout;

class App extends React.Component {
  componentDidMount() {
    const { fetchCampaigns, userId, initialFetch } = this.props;
    if (userId && initialFetch) {
      fetchCampaigns(userId);
    }
  }

  componentDidUpdate(prevProps) {
    const { fetchCampaigns, userId } = this.props;
    if (userId && prevProps.userId !== userId) {
      fetchCampaigns(userId);
    }
  }

  render() {
    return (
      <Layout style={{ width: '310px', height: '600px', overflowY: 'scroll' }}>
        <Header style={{ color: '#ffffff', padding: '0 10px', textAlign: 'center' }}>
          <span>Bolster Naming Convention / Pixel Generator</span>
        </Header>
        <Content style={{ padding: '0 25px 25px', height: '100%', overflow: 'scroll' }}>
          {/* <ErrorManager /> */}
          <ContentGeneratorForm />
        </Content>
      </Layout>
    );
  }
}

App.defaultProps = {
  userId: null,
};

App.propTypes = {
  userId: PropTypes.string,
  initialFetch: PropTypes.bool.isRequired,
  fetchCampaigns: PropTypes.func.isRequired,
};

const mapStateToProps = ({ user, campaigns }) => ({
  userId: user.id,
  initialFetch: campaigns.campaigns.length === 0,
});

const mapDispatchToProps = dispatch => ({
  fetchCampaigns: userId => dispatch({ type: actions.FETCH_CAMPAIGNS, userId }),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
