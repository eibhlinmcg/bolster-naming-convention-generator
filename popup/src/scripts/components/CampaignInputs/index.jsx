import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Form, Select, Input, Checkbox, Button,
} from 'antd';
import Loader from '../Loader';
import { formItemStyle } from '../../styles';
import actions from '../../../../../constants/actionsTypes/campaigns';

const FormItem = Form.Item;
const { Option } = Select;

const inputs = {
  CLIENT: 'client',
  CAMPAIGN: 'campaign',
  CODE: 'code',
};

class CampaignInputs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredCampaigns: props.campaigns,
      manualEntry: props.shouldEnterManually,
    };
    this.handleClientChange = this.handleClientChange.bind(this);
    this.handleCampaignChange = this.handleCampaignChange.bind(this);
    this.toggleManualEntry = this.toggleManualEntry.bind(this);
    this.refetchCampaigns = this.refetchCampaigns.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { form, campaigns, shouldEnterManually } = this.props;
    const { filteredCampaigns } = this.state;
    if (filteredCampaigns.length !== campaigns.length && !form.isFieldsTouched()) {
      this.setState({ filteredCampaigns: campaigns });
    }
    if (prevProps.shouldEnterManually !== shouldEnterManually) {
      this.setState({ manualEntry: shouldEnterManually });
    }
  }

  toggleManualEntry() {
    const { campaigns, form } = this.props;
    form.setFieldsValue({ [inputs.CAMPAIGN]: null, [inputs.CLIENT]: null, [inputs.CODE]: null });
    this.setState(prevState => ({
      manualEntry: !prevState.manualEntry,
      filteredCampaigns: campaigns,
    }));
  }

  handleClientChange(value) {
    const { campaigns, form } = this.props;
    const filteredCampaigns = campaigns.filter(({ client }) => client === value);
    const activeCampaign = filteredCampaigns.length === 1 ? filteredCampaigns[0] : null;
    form.setFieldsValue({
      [inputs.CAMPAIGN]: activeCampaign && activeCampaign.id,
      [inputs.CLIENT]: value,
      [inputs.CODE]: activeCampaign && activeCampaign.code,
    });
    this.setState({ filteredCampaigns });
  }

  handleCampaignChange(value) {
    const { campaigns, form } = this.props;
    const selectedCampaign = campaigns.find(({ id }) => value === id);
    form.setFieldsValue({
      [inputs.CAMPAIGN]: value,
      [inputs.CLIENT]: selectedCampaign ? selectedCampaign.client : null,
      [inputs.CODE]: selectedCampaign ? selectedCampaign.code : null,
    });
  }

  refetchCampaigns() {
    const { userId, fetchCampaigns } = this.props;
    fetchCampaigns(userId);
  }

  render() {
    const {
      fetching,
      clients,
      shouldEnterManually,
      form: { getFieldValue, getFieldDecorator },
    } = this.props;
    const { filteredCampaigns, manualEntry } = this.state;
    if (fetching && !manualEntry) return <Loader data-test="Loader" />;
    return (
      <section data-test="CampaignFormInputs">
        <FormItem
          data-test="ClientFormItem"
          label={`${manualEntry ? '' : 'Select '}Client`}
          style={formItemStyle}
        >
          {getFieldDecorator(inputs.CLIENT)(
            manualEntry ? (
              <Input data-test="ClientInput" placeholder="BLSTR" value={getFieldValue(inputs.CLIENT)} />
            ) : (
              <Select
                showSearch
                placeholder="Select client"
                data-test="ClientSelect"
                value={getFieldValue(inputs.CLIENT)}
                onChange={this.handleClientChange}
              >
                {clients.map(({ id, name }) => <Option key={id} value={id}>{name}</Option>)}
              </Select>
            ),
          )}
        </FormItem>
        <FormItem
          data-test="CampaignFormItem"
          label={`${manualEntry ? '' : 'Select '}Campaign`}
          style={formItemStyle}
        >
          {getFieldDecorator(inputs.CAMPAIGN, {
            rules: [{ required: true, message: 'A campaign is required' }],
          })(
            manualEntry ? (
              <Input placeholder="Laneway" value={getFieldValue(inputs.CAMPAIGN)} />
            ) : (
              <Select
                showSearch
                placeholder="Select campaign"
                value={getFieldValue(inputs.CAMPAIGN)}
                onChange={this.handleCampaignChange}
              >
                {filteredCampaigns.map(({ id, name }) => (
                  <Option key={id} value={id}>{name}</Option>
                ))}
              </Select>
            ),
          )}
        </FormItem>
        <FormItem label="Campaign code" style={formItemStyle}>
          {getFieldDecorator(inputs.CODE, {
            rules: [{ required: true, message: 'A campaign code is required' }],
          })(
            <Input readOnly={!manualEntry} />,
          )}
        </FormItem>
        {!shouldEnterManually && (
          <div
            data-test="ActionButtons"
            style={{
              ...formItemStyle,
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Button icon="sync" data-test="RefetchButton" onClick={this.refetchCampaigns} />
            <Checkbox data-test="ManualEntryCheckBox" checked={manualEntry} onChange={this.toggleManualEntry}>
              Enter Manually
            </Checkbox>
          </div>
        )}
      </section>
    );
  }
}

CampaignInputs.defaultProps = {
  userId: null,
};

CampaignInputs.propTypes = {
  fetching: PropTypes.bool.isRequired,
  shouldEnterManually: PropTypes.bool.isRequired,
  campaigns: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    client: PropTypes.string.isRequired,
  })).isRequired,
  clients: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  })).isRequired,
  form: PropTypes.shape({
    setFieldsValue: PropTypes.func.isRequired,
    getFieldDecorator: PropTypes.func.isRequired,
    getFieldValue: PropTypes.func.isRequired,
  }).isRequired,
  userId: PropTypes.string,
  fetchCampaigns: PropTypes.func.isRequired,
};

const mapStateToProps = ({ user, campaigns }) => ({
  shouldEnterManually: !user.id || campaigns.error,
  userId: user.id,
  ...campaigns,
});

const mapDispatchToProps = dispatch => ({
  fetchCampaigns: userId => dispatch({ type: actions.FETCH_CAMPAIGNS, userId }),
});

export default connect(mapStateToProps, mapDispatchToProps)(CampaignInputs);
