import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Tabs, Icon } from 'antd';
import CampaignInputs from '../CampaignInputs';
import GeneratedStringCopy from '../GeneratedStringCopy';
import { formItemStyle } from '../../styles';
import tabs from '../../../constants/tabs';
import actions from '../../../../../constants/actionsTypes/user';
import inputs from '../../../constants/inputs';

const { TabPane } = Tabs;

class ContentGeneratorForm extends React.Component {
  componentDidMount() {
    const { form: { setFieldsValue }, inputValues } = this.props;
    setFieldsValue(inputValues);
  }

  render() {
    const { form, activeGenerator, updateActiveTab } = this.props;
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Form style={formItemStyle}>
          <CampaignInputs form={form} />
          <Tabs
            activeKey={activeGenerator}
            onChange={updateActiveTab}
            tabBarStyle={{ display: 'flex', justifyContent: 'center' }}
          >
            {tabs.map(({ icon, generator, Component }) => (
              <TabPane tab={<Icon type={icon} />} key={generator}>
                {activeGenerator === generator && (
                  <Component
                    form={form}
                    inputs={inputs[activeGenerator]}
                  />
                )}
              </TabPane>
            ))}
          </Tabs>
        </Form>
        <GeneratedStringCopy />
      </div>
    );
  }
}

ContentGeneratorForm.propTypes = {
  form: PropTypes.shape({
    validateFields: PropTypes.func.isRequired,
    setFieldsValue: PropTypes.func.isRequired,
  }).isRequired,
  activeGenerator: PropTypes.string.isRequired,
  inputValues: PropTypes.shape({}).isRequired,
  updateActiveTab: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { user: { activeGenerator } } = state;
  return {
    activeGenerator,
    ...state[activeGenerator],
  };
};

const mapDispatchToProps = dispatch => ({
  updateActiveTab: (activeGenerator) => {
    dispatch({ type: actions.CHANGE_ACTIVE_TAB, activeGenerator });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(ContentGeneratorForm));
