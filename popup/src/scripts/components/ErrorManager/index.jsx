import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const errorMessageStyle = {
  fontSize: '12px',
  lineHeight: '14px',
  color: 'red',
  marginTop: '15px',
  display: 'block',
};

const openOptions = () => {
  chrome.runtime.openOptionsPage();
};

const ErrorManager = ({ showLoginWarning, campaignsError }) => (
  <section>
    {showLoginWarning && (
      <a data-test="LoginWarning" onClick={openOptions} style={errorMessageStyle}>
        {"Cannot access your current campaigns or clients as you haven't signed into Agency OS. Click here to sign in."}
      </a>
    )}
    {campaignsError && (
      <span style={errorMessageStyle} data-test="CampaignsErrorMessage">
        {`There was a problem fetching the campaigns: ${campaignsError}`}
      </span>
    )}
  </section>
);

ErrorManager.propTypes = {
  showLoginWarning: PropTypes.bool.isRequired,
  campaignsError: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
};

const mapStateToProps = ({ user, campaigns }) => ({
  showLoginWarning: user.id == null,
  campaignsError: campaigns.error,
});

export default connect(mapStateToProps)(ErrorManager);
