import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import copy from 'copy-to-clipboard';
import { Button, Alert } from 'antd';

class GeneratedStringCopy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false,
    };
    this.copyGeneratedString = this.copyGeneratedString.bind(this);
  }

  componentDidUpdate() {
    const { copied } = this.state;
    if (copied) {
      this.removeCopyMessage = setTimeout(() => {
        this.setState({ copied: false });
      }, 3000);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.removeCopyMessage);
  }

  copyGeneratedString() {
    const { generatedString } = this.props;
    copy(generatedString);
    this.setState({ copied: true });
  }

  render() {
    const { copied } = this.state;
    const { generatedString } = this.props;
    if (!generatedString) return null;
    return (
      <section style={{ margin: '20px' }}>
        <p>Click to copy:</p>
        <Button
          onClick={this.copyGeneratedString}
          data-test="CopyButton"
          type="dashed"
          style={{
            width: '100%',
            height: '100%',
            overflowWrap: 'break-word',
            wordBreak: 'break-all',
            whiteSpace: 'normal',
            textAlign: 'left',
            padding: '15px',
          }}
        >
          {generatedString}
        </Button>
        {copied && (
          <Alert
            data-test="CopiedAlert"
            style={{
              position: 'fixed',
              top: '20px',
              left: '20px',
              right: '20px',
            }}
            message="Copied to clipboard"
            type="success"
          />
        )}
      </section>
    );
  }
}

GeneratedStringCopy.defaultProps = {
  generatedString: false,
};

GeneratedStringCopy.propTypes = {
  generatedString: PropTypes.string,
};

const mapStateToProps = (store) => {
  const { user: { activeGenerator } } = store;
  return {
    ...store[activeGenerator],
  };
};

export default connect(mapStateToProps)(GeneratedStringCopy);
