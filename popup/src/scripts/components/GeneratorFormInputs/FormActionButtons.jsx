import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

const FormActionButtons = ({ onSubmit, showReset, onReset }) => (
  <section>
    <Button
      data-test="SubmitButton"
      type="primary"
      htmlType="submit"
      onClick={onSubmit}
    >
      Generate & copy
    </Button>
    {showReset && (
      <Button
        data-test="ResetButton"
        type="secondary"
        style={{ marginLeft: '15px' }}
        onClick={onReset}
      >
        Reset
      </Button>
    )}
  </section>
);

FormActionButtons.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  showReset: PropTypes.bool.isRequired,
  onReset: PropTypes.func.isRequired,
};

export default FormActionButtons;
