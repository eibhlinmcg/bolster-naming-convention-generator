import React from 'react';
import PropTypes from 'prop-types';
import {
  Form, Select, Input,
} from 'antd';
import { formItemStyle } from '../../styles';

const FormItem = Form.Item;
const { Option } = Select;

const CUSTOM_INPUT = '_CUSTOM';

const FormInput = ({
  input: {
    key, label, rules, options, placeholder, hasCustom,
  },
  getFieldDecorator,
  getFieldValue,
}) => (
  <section>
    <FormItem label={label} style={formItemStyle}>
      {getFieldDecorator(key, { rules })(options ? (
        <Select showSearch placeholder={placeholder}>
          {options.map(({ id, displayName, name }) => <Option key={`${key}-${id}`} value={name}>{displayName || name}</Option>)}
          {hasCustom && <Option key={`${key}-${options.length}`} value={CUSTOM_INPUT}>Custom / Other</Option>}
        </Select>
      ) : (
        <Input placeholder={placeholder} />
      ))}
    </FormItem>
    {hasCustom && getFieldValue(key) === CUSTOM_INPUT && (
      <FormItem>
        {getFieldDecorator(`${key}${CUSTOM_INPUT}`, { rules })(<Input placeholder={placeholder} />)}
      </FormItem>
    )}
  </section>
);

FormInput.propTypes = {
  input: PropTypes.shape({
    label: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    rules: PropTypes.arrayOf(PropTypes.shape({})),
    placeholder: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      diplsayName: PropTypes.string,
    })),
    hasCustom: PropTypes.bool,
  }).isRequired,
  getFieldValue: PropTypes.func.isRequired,
  getFieldDecorator: PropTypes.func.isRequired,
};

export default FormInput;
