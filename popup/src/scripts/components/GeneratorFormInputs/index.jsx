import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form } from 'antd';
import FormInput from './FormInput';
import FormActionButtons from './FormActionButtons';
import actions from '../../../../../constants/actionsTypes';
import { formItemStyle } from '../../styles';

const FormItem = Form.Item;

class GeneratorFormInputs extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  componentDidMount() {
    const { form: { setFieldsValue }, inputValues } = this.props;
    setFieldsValue(inputValues);
  }

  handleSubmit() {
    const { form, submitAndGenerateCode, activeGenerator } = this.props;
    form.validateFields((errors, values) => {
      if (errors) return;
      submitAndGenerateCode(values, activeGenerator);
    });
  }

  handleReset() {
    const { form: { resetFields }, reset, activeGenerator } = this.props;
    resetFields();
    reset(activeGenerator);
  }

  render() {
    const { form, inputs } = this.props;
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {Object.keys(inputs).map((input, index) => (
          <FormInput
            key={`${input}-${index}`}
            input={inputs[input]}
            getFieldValue={form.getFieldValue}
            getFieldDecorator={form.getFieldDecorator}
          />
        ))}
        <FormItem style={formItemStyle}>
          <FormActionButtons
            onSubmit={this.handleSubmit}
            onReset={this.handleReset}
            showReset={form.isFieldsTouched()}
          />
        </FormItem>
      </div>
    );
  }
}

GeneratorFormInputs.propTypes = {
  form: PropTypes.shape({
    setFieldsValue: PropTypes.func.isRequired,
  }).isRequired,
  inputs: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  inputValues: PropTypes.shape({}).isRequired,
  submitAndGenerateCode: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  activeGenerator: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  const { user: { activeGenerator } } = state;
  return {
    activeGenerator,
    ...state[activeGenerator],
  };
};

const mapDispatchToProps = dispatch => ({
  submitAndGenerateCode: (inputValues, activeGenerator) => (
    dispatch({ type: actions[activeGenerator].GENERATE_STRING, inputValues })
  ),
  reset: activeGenerator => dispatch({ type: actions[activeGenerator].RESET_STATE }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(GeneratorFormInputs));
