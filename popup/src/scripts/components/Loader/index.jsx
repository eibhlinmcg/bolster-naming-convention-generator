import React from 'react';
import { Icon } from 'antd';

export default () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      width: '100%',
      height: '250px',
      padding: '20px',
      fontSize: '12px',
    }}
  >
    <Icon type="loading" style={{ fontSize: '20px' }} />
    <span>Fetching your clients and campaigns</span>
  </div>
);
