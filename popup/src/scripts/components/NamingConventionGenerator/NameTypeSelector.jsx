import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Select } from 'antd';
import { nameTypeOptions } from '../../../../../constants/nameTypes';
import actions from '../../../../../constants/actionsTypes/name';

const { Option } = Select;

const NameTypeSelector = ({ selectNameType, selectedNameType }) => (
  <section>
    <h4>What do you want to name?</h4>
    <div style={{ display: 'flex', alignItems: 'flex-end' }}>
      <Select
        data-test="NameType-Select"
        showSearch
        size="large"
        style={{ width: '170px' }}
        onChange={selectNameType}
        placeholder="Select one"
        value={selectedNameType && selectedNameType}
      >
        {nameTypeOptions.map(({ id, name, key }) => (
          <Option
            key={`NameTypeOption-${id}`}
            value={key}
          >
            {name}
          </Option>
        ))}
      </Select>
    </div>
  </section>
);

NameTypeSelector.defaultProps = {
  selectedNameType: false,
};

NameTypeSelector.propTypes = {
  selectedNameType: PropTypes.string,
  selectNameType: PropTypes.func.isRequired,
};

const mapStateToProps = ({ name }) => ({
  selectedNameType: name.activeNameType,
});

const mapDispatchToProps = dispatch => ({
  selectNameType: nameType => dispatch({ type: actions.SET_NAME_TYPE, nameType }),
});

export default connect(mapStateToProps, mapDispatchToProps)(NameTypeSelector);
