import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form } from 'antd';
import GeneratorFormInputs from '../GeneratorFormInputs';
import NameTypeSelector from './NameTypeSelector';

const NamingConventionGenerator = ({ form, inputs, activeNameType }) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    <NameTypeSelector />
    {activeNameType && (
      <GeneratorFormInputs
        form={form}
        data-test="GeneratorFormInputs"
        inputs={inputs[activeNameType]}
      />
    )}
  </div>
);

NamingConventionGenerator.defaultProps = {
  activeNameType: false,
};

NamingConventionGenerator.propTypes = {
  form: PropTypes.shape({
  }).isRequired,
  activeNameType: PropTypes.string,
  inputs: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => state.name;

export default connect(mapStateToProps)(Form.create()(NamingConventionGenerator));
