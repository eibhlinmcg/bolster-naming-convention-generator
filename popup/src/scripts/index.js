import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'react-chrome-redux';
import 'antd/dist/antd.css'
import App from './App';
import portName from '../../../constants/portName';

const store = new Store({ portName });

store.ready().then(() => {
  render(
    <Provider store={store}>
      <App style={{ height: '100%' }}/>
    </Provider>
    , document.getElementById('app'));
});