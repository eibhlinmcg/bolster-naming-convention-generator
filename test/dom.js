const jsdom = require('jsdom');

const exposedProperties = ['window', 'navigator', 'document'];

const { document } = (new jsdom.JSDOM('<html><body></body></html>', { url: 'https://example.com' })).window;
global.document = document;
global.window = document.defaultView;
Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js',
};
