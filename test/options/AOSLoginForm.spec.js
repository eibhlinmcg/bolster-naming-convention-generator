import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import AOSLoginForm from '../../options/src/scripts/components/AOSLoginForm';

configure({ adapter: new Adapter() });

const authenticationError = 'Error';

describe('AOSLoginForm Component', () => {
  let wrapper;
  let loginSpy;

  before(() => {
    loginSpy = sinon.spy();

    wrapper = shallow(
      <AOSLoginForm
        login={loginSpy}
        authenticating={false}
        authenticationError={false}
      />,
    ).dive();
  });

  it('should render Loading Icon when authenticating prop is true', () => {
    wrapper.setProps({ authenticating: true });
    expect(wrapper.find({ 'data-test': 'LoadingIcon' }).exists()).to.eql(true);
  });

  it('should not render Loading Icon when authenticating prop is false', () => {
    wrapper.setProps({ authenticating: false });
    expect(wrapper.find({ 'data-test': 'LoadingIcon' }).exists()).to.eql(false);
  });

  it('should render LoginButtonFormItem when authenticating prop is false', () => {
    wrapper.setProps({ authenticating: false });
    expect(wrapper.find({ 'data-test': 'LoginButtonFormItem' }).exists()).to.eql(true);
  });

  it('should not render LoginButtonFormItem Icon when authenticating prop is true', () => {
    wrapper.setProps({ authenticating: true });
    expect(wrapper.find({ 'data-test': 'LoginButtonFormItem' }).exists()).to.eql(false);
  });

  it('should render the error message if there is an authenication error passed as a prop', () => {
    wrapper.setProps({ authenticating: false, authenticationError });
    expect(wrapper.find({ 'data-test': 'LoginButtonFormItem' }).prop('help').props.children).to.eql(authenticationError);
  });
});
