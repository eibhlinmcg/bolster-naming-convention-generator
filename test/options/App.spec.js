import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../../options/src/scripts/App';

configure({ adapter: new Adapter() });

describe('App Component', () => {
  let wrapper;

  before(() => {
    wrapper = shallow(
      <App.WrappedComponent
        login={() => {}}
        logout={() => {}}
        authenticating={false}
        authenticationError={false}
      />,
    );
  });

  it('should render the LoginForm if no token is passed as a prop', () => {
    wrapper.setProps({ token: null });
    expect(wrapper.find({ 'data-test': 'AOSLoginForm' }).exists()).to.eql(true);
  });

  it('should not render the LoginForm if a token is passed as a prop', () => {
    wrapper.setProps({ token: '1234' });
    expect(wrapper.find({ 'data-test': 'AOSLoginForm' }).exists()).to.eql(false);
  });

  it('should not render the UserProfile if no token is passed as a prop', () => {
    wrapper.setProps({ token: null });
    expect(wrapper.find({ 'data-test': 'UserProfile' }).exists()).to.eql(false);
  });

  it('should render the UserProfile if a token is passed as a prop', () => {
    wrapper.setProps({ token: '1234' });
    expect(wrapper.find({ 'data-test': 'UserProfile' }).exists()).to.eql(true);
  });
});
