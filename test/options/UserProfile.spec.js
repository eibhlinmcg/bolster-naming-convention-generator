import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import UserProfile from '../../options/src/scripts/components/UserProfile';

configure({ adapter: new Adapter() });

const firstName = 'Bronson';
const lastName = 'Stark';
const avatarUrl = 'https://doggo.png';

describe('UserProfile Component', () => {
  let wrapper;
  let logoutSpy;

  before(() => {
    logoutSpy = sinon.spy();

    wrapper = shallow(
      <UserProfile
        firstName={firstName}
        lastName={lastName}
        avatarUrl={avatarUrl}
        logout={logoutSpy}
      />,
    );
  });

  it('should render initials for the avatar', () => {
    expect(wrapper.find({ 'data-test': 'AvatarInitials' }).text()).to.eql('B S');
  });

  it('should render name in the header', () => {
    expect(wrapper.find({ 'data-test': 'NameHeader' }).text()).to.eql('Bronson Stark');
  });

  it('should call the logout function on click on the logout button', () => {
    wrapper.find({ 'data-test': 'LogoutButton' }).simulate('click');
    expect(logoutSpy.calledOnce).to.eql(true);
  });
});
