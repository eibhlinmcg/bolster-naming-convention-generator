import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import App from '../../popup/src/scripts/App';

configure({ adapter: new Adapter() });

describe('App Component - userId & initialFetch', () => {
  let wrapper;
  let fetchCampaignsSpy;

  before(() => {
    fetchCampaignsSpy = sinon.spy();
    wrapper = shallow(
      <App.WrappedComponent
        userId="1234"
        initialFetch
        fetchCampaigns={fetchCampaignsSpy}
      />,
    );
  });

  after(() => {
    fetchCampaignsSpy = null;
  });

  it('should call the fetchCampaigns function on render if userId is passed as a prop & initialFetch is true', () => {
    expect(fetchCampaignsSpy.calledOnceWith('1234')).to.eql(true);
  });

  it('should call the fetchCampaigns function twice if the userId is updated', () => {
    wrapper.setProps({ userId: '456' });
    expect(fetchCampaignsSpy.calledTwice).to.eql(true);
  });
});


describe('App Component - userId & !initialFetch', () => {
  let wrapper;
  let fetchCampaignsSpy;

  beforeEach(() => {
    fetchCampaignsSpy = sinon.spy();
    wrapper = shallow(
      <App.WrappedComponent
        userId="1234"
        initialFetch={false}
        fetchCampaigns={fetchCampaignsSpy}
      />,
    );
  });

  afterEach(() => {
    fetchCampaignsSpy = null;
  });

  it('should not call the fetchCampaigns function on render if initialFetch is false', () => {
    expect(fetchCampaignsSpy.called).to.eql(false);
  });

  it('should call the fetchCampaigns function if the userId prop is updated', () => {
    wrapper.setProps({ userId: '456' });
    expect(fetchCampaignsSpy.calledOnceWith('456')).to.eql(true);
  });

  it('should not call the fetchCampaigns function if the userId prop is updated to null', () => {
    wrapper.setProps({ userId: null });
    expect(fetchCampaignsSpy.calledOnce).to.eql(false);
  });
});

describe('App Component - !userId & initialFetch', () => {
  let wrapper;
  let fetchCampaignsSpy;

  before(() => {
    fetchCampaignsSpy = sinon.spy();
    wrapper = shallow(
      <App.WrappedComponent
        initialFetch
        fetchCampaigns={fetchCampaignsSpy}
      />,
    );
  });

  after(() => {
    fetchCampaignsSpy = null;
  });

  it('should not call the fetchCampaigns function on render if userId is not passed as prop', () => {
    expect(fetchCampaignsSpy.called).to.eql(false);
  });

  it('should call the fetchCampaigns function if the userId prop is updated', () => {
    wrapper.setProps({ userId: '456' });
    expect(fetchCampaignsSpy.calledOnceWith('456')).to.eql(true);
  });
});
