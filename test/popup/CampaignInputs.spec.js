import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import CampaignInputs from '../../popup/src/scripts/components/CampaignInputs';

const userId = '1234';
const BLSTR = 'BLSTR';
const LNWY = 'LNWY';

const campaigns = [{
  name: 'Festival 1', code: 'F1', id: '0', client: BLSTR,
}, {
  name: 'Festival 2', code: 'F2', id: '1', client: BLSTR,
}, {
  name: 'Festival 3', code: 'F3', id: '2', client: BLSTR,
}, {
  name: 'Festival 4', code: 'F4', id: '3', client: LNWY,
}, {
  name: 'Festival 5', code: 'F5', id: '4', client: BLSTR,

}];

const clients = [{
  name: BLSTR,
  code: BLSTR,
  id: '0',
}, {
  name: LNWY,
  code: LNWY,
  id: '1',
}];

configure({ adapter: new Adapter() });

describe('CampaignInputs Component - Should Enter Manually', () => {
  let wrapper;
  let getFieldDecoratorStub;

  beforeEach(() => {
    getFieldDecoratorStub = sinon.stub();
    getFieldDecoratorStub.returns(() => {});
    wrapper = shallow(
      <CampaignInputs.WrappedComponent
        fetching={false}
        shouldEnterManually
        campaigns={[]}
        clients={[]}
        form={{
          setFieldsValue: () => {},
          getFieldDecorator: getFieldDecoratorStub,
          getFieldValue: () => {},
        }}
        fetchCampaigns={() => {}}

      />,
    );
  });

  it('should not render the loader', () => {
    expect(wrapper.find({ 'data-test': 'Loader' }).exists()).to.eql(false);
  });

  it('should render the text Client on the client form item label prop', () => {
    expect(wrapper.find({ 'data-test': 'ClientFormItem' }).prop('label')).to.eql('Client');
  });

  it('should render the text Campaign on the client form item label prop', () => {
    wrapper.setState({ manualEntry: true });
    expect(wrapper.find({ 'data-test': 'CampaignFormItem' }).prop('label')).to.eql('Campaign');
  });

  it('should not display the action buttons if the shouldEnterManually prop is true', () => {
    expect(wrapper.find({ 'data-test': 'ActionButtons' }).exists()).to.eql(false);
  });
});

describe('CampaignInputs Component - fethed campaigns', () => {
  let wrapper;
  let setFieldsValueSpy;
  let getFieldDecoratorStub;
  let getFieldValueSpy;
  let fetchCampaignsSpy;
  let isFieldsTouchedStub;


  beforeEach(() => {
    setFieldsValueSpy = sinon.spy();
    getFieldDecoratorStub = sinon.stub();
    getFieldDecoratorStub.returns(() => {});
    getFieldValueSpy = sinon.spy();
    fetchCampaignsSpy = sinon.spy();
    isFieldsTouchedStub = sinon.stub();
    isFieldsTouchedStub.returns(true);
    wrapper = shallow(
      <CampaignInputs.WrappedComponent
        fetching={false}
        shouldEnterManually={false}
        campaigns={campaigns}
        clients={clients}
        form={{
          setFieldsValue: setFieldsValueSpy,
          getFieldDecorator: getFieldDecoratorStub,
          getFieldValue: getFieldValueSpy,
          isFieldsTouched: isFieldsTouchedStub,
        }}
        userId="1234"
        fetchCampaigns={fetchCampaignsSpy}

      />,
    );
  });

  afterEach(() => {
    setFieldsValueSpy = null;
    getFieldValueSpy = null;
    fetchCampaignsSpy = null;
  });

  it('should render the loader if fetching props is true', () => {
    wrapper.setProps({ fetching: true });
    expect(wrapper.find({ 'data-test': 'Loader' }).exists()).to.eql(true);
  });

  it('should not render the campaign form inputs if fetching props is true and shouldEnterManually prop is false', () => {
    wrapper.setProps({ fetching: true });
    expect(wrapper.find({ 'data-test': 'CampaignFormInputs' }).exists()).to.eql(false);
  });

  it('should render the text Select Client on the client form item label prop', () => {
    expect(wrapper.find({ 'data-test': 'ClientFormItem' }).prop('label')).to.eql('Select Client');
  });

  it('should render the text Select Campaign on the client form item label prop', () => {
    expect(wrapper.find({ 'data-test': 'CampaignFormItem' }).prop('label')).to.eql('Select Campaign');
  });

  it('should display the action buttons', () => {
    expect(wrapper.find({ 'data-test': 'ActionButtons' }).exists()).to.eql(true);
  });

  it('should call the fetchCampaigns function if the RefetchButton is clicked', () => {
    wrapper.find({ 'data-test': 'RefetchButton' }).simulate('click');
    expect(fetchCampaignsSpy.calledOnceWith(userId)).to.eql(true);
  });

  it('should set the manualEntry state to true if the manualEntry text box is clicked', () => {
    wrapper.find({ 'data-test': 'ManualEntryCheckBox' }).simulate('change');
    expect(wrapper.state('manualEntry')).to.eql(true);
  });


  it('should set the field values to null when the manual entry checkbox is clicked', () => {
    wrapper.find({ 'data-test': 'ManualEntryCheckBox' }).simulate('change');

    expect(setFieldsValueSpy.calledOnceWith({
      campaign: null, client: null, code: null,
    })).to.eql(true);
  });

  it('should set the filteredCampaigns to the props campaigns when the manual entry checkbox is clicked', () => {
    wrapper.setState({ filteredCampaigns: [campaigns[0]] });
    wrapper.find({ 'data-test': 'ManualEntryCheckBox' }).simulate('change');
    expect(wrapper.state('filteredCampaigns').length).to.eql(campaigns.length);
  });

  it('should set the client field to the value arg and the campaign and code values to null when the handleClientChange function is called with a client value that has >1 campaign', () => {
    wrapper.instance().handleClientChange(BLSTR);
    expect(setFieldsValueSpy.calledOnceWith({
      campaign: null,
      client: BLSTR,
      code: null,
    })).to.eql(true);
  });

  it('should set the client field to the value arg and the cmapaign and code values when the handleClientChange function is called with a client value that has only 1 campaign', () => {
    wrapper.instance().handleClientChange(LNWY);
    expect(setFieldsValueSpy.calledOnceWith({
      campaign: '3',
      client: LNWY,
      code: 'F4',
    })).to.eql(true);
  });

  it('should update the filtered campaigns state when the handleClientChange function is called', () => {
    wrapper.instance().handleClientChange(BLSTR);
    expect(wrapper.state('filteredCampaigns').length).to.eql(campaigns.filter(({ client }) => client === BLSTR).length);
  });

  it('should set the campaign field to the value arg and the corresponding campaign and code fields when handleCampaignChange is called', () => {
    wrapper.instance().handleCampaignChange('0');
    expect(setFieldsValueSpy.calledOnceWith({
      campaign: '0',
      client: BLSTR,
      code: 'F1',
    })).to.eql(true);
  });
});
