import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ErrorManager from '../../popup/src/scripts/components/ErrorManager';

configure({ adapter: new Adapter() });

const campaignsError = 'Error retrieving cmapigns';

describe('ErrorManager Component', () => {
  let wrapper;
  before(() => {
    wrapper = shallow(
      <ErrorManager.WrappedComponent
        showLoginWarning={false}
        campaignsError="Error"
      />,
    );
  });

  it('should render the login warning if showLoginWarning prop is true', () => {
    wrapper.setProps({ showLoginWarning: true });
    expect(wrapper.find({ 'data-test': 'LoginWarning' }).exists()).to.eql(true);
  });

  it('should not render the login warning if showLoginWarning prop is false', () => {
    wrapper.setProps({ showLoginWarning: false });
    expect(wrapper.find({ 'data-test': 'LoginWarning' }).exists()).to.eql(false);
  });

  it('should render the camapaigns error message if campaignsError prop is a string', () => {
    wrapper.setProps({ campaignsError });
    expect(wrapper.find({ 'data-test': 'CampaignsErrorMessage' }).exists()).to.eql(true);
  });

  it('should not render the camapaigns error message if campaignsError prop is false', () => {
    wrapper.setProps({ campaignsError: false });
    expect(wrapper.find({ 'data-test': 'CampaignsErrorMessage' }).exists()).to.eql(false);
  });

  it('should not render the camapaigns error message text if campaignsError prop is a string', () => {
    wrapper.setProps({ campaignsError });
    expect(wrapper.find({ 'data-test': 'CampaignsErrorMessage' }).text().includes(campaignsError)).to.eql(true);
  });
});
