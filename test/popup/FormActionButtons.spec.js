import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import FormActionButtons from '../../popup/src/scripts/components/GeneratorFormInputs/FormActionButtons';

configure({ adapter: new Adapter() });

describe('FormActionButtons Component', () => {
  let wrapper;
  let onSubmitSpy;
  let onResetSpy;

  beforeEach(() => {
    onSubmitSpy = sinon.spy();
    onResetSpy = sinon.spy();
    wrapper = shallow(
      <FormActionButtons
        onSubmit={onSubmitSpy}
        showReset
        onReset={onResetSpy}
      />,
    );
  });

  it('should call the onSubmit prop when the submit button is clicked', () => {
    wrapper.find({ 'data-test': 'SubmitButton' }).simulate('click');
    expect(onSubmitSpy.calledOnce).to.eql(true);
  });


  it('should render the Reset Button if showReset prop is true', () => {
    wrapper.setProps({ showReset: true });
    expect(wrapper.find({ 'data-test': 'ResetButton' }).exists()).to.eql(true);
  });

  it('should not render the Reset Button if showReset prop is false', () => {
    wrapper.setProps({ showReset: false });
    expect(wrapper.find({ 'data-test': 'ResetButton' }).exists()).to.eql(false);
  });

  it('should call the onReset prop when the reset button is clicked', () => {
    wrapper.find({ 'data-test': 'ResetButton' }).simulate('click');
    expect(onResetSpy.calledOnce).to.eql(true);
  });
});
