import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GeneratedStringCopy from '../../popup/src/scripts/components/GeneratedStringCopy';

configure({ adapter: new Adapter() });

const generatedString = 'X - Y - Z';

describe('GeneratedStringCopy Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <GeneratedStringCopy.WrappedComponent
        generatedString={generatedString}
      />,
    );
  });

  it('should render null if the generatedString prop is null', () => {
    wrapper.setProps({ generatedString: null });
    expect(wrapper.get(0)).to.eql(null);
  });

  it('should not render the CopiedAlert on render', () => {
    expect(wrapper.find({ 'data-test': 'CopiedAlert' }).exists()).to.eql(false);
  });

  it('should render the CopiedAlert when the copied state is true', () => {
    wrapper.setState({ copied: true });
    expect(wrapper.find({ 'data-test': 'CopiedAlert' }).exists()).to.eql(true);
  });

  it('should reset the copied state to false after 3 seconds', () => {
    wrapper.setState({ copied: true });
    setTimeout(() => {
      expect(wrapper.state('copied')).to.eql(false);
    }, 3001);
  });
});
