import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import NameTypeSelector from '../../popup/src/scripts/components/NamingConventionGenerator/NameTypeSelector';

configure({ adapter: new Adapter() });

const selectedNameType = 'Campaign Name';

describe('NameTypeSelector Component', () => {
  let wrapper;
  let selectNameTypeSpy;

  beforeEach(() => {
    selectNameTypeSpy = sinon.spy();
    wrapper = shallow(
      <NameTypeSelector.WrappedComponent
        selectedNameType={selectedNameType}
        selectNameType={selectNameTypeSpy}
      />,
    );
  });

  afterEach(() => {
    selectNameTypeSpy = null;
  });

  it('should call the seelctNameType function when select is changed', () => {
    wrapper.find({ 'data-test': 'NameType-Select' }).simulate('change', 'ad');
    expect(selectNameTypeSpy.calledOnceWith('ad')).to.eql(true);
  });

  it('should set the value on thr NameType select to be the selectedNameType passed as props', () => {
    expect(wrapper.find({ 'data-test': 'NameType-Select' }).prop('value')).to.eql(selectedNameType);
  });
});
