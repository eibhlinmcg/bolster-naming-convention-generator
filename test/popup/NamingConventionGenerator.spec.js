import React from 'react';
import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import NamingConventionGenerator from '../../popup/src/scripts/components/NamingConventionGenerator';

configure({ adapter: new Adapter() });

describe('NamingConventionGenerator Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <NamingConventionGenerator.WrappedComponent
        form={{}}
        activeNameType="CAMPAIGN_NAME"
        inputs={{}}
      />,
    );
  });

  it('should render the GeneratorFormInputs if an activeNameType is passed as a prop', () => {
    expect(wrapper.find({ 'data-test': 'GeneratorFormInputs' }).exists()).to.eql(true);
  });

  it('should not render the GeneratorFormInputs if no activeNameType is passed as a prop', () => {
    wrapper.setProps({ activeNameType: null });
    expect(wrapper.find({ 'data-test': 'GeneratorFormInputs' }).exists()).to.eql(false);
  });
});
