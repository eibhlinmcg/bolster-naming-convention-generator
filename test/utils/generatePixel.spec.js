import { expect } from 'chai';
import generatePixel from '../../utils/generatePixel';

const pixel = '1234';
const event = 'View content';
const eventInitialScript = 'Page view';
const eventAdditionalTracking = 'Make purchase';
const campaignCode = 'LNWY';

describe('generatePixel', () => {
  before(() => {
  });

  it('should render the correct script', () => {
    expect(generatePixel({ pixel, event, campaignCode })).to.eql(
      '<script>\n'
      + `fbq('track', 'ViewContent', { campaign: '${campaignCode}' });\n`
      + '</script>\n',
    );
  });

  it('should render the correct initial script', () => {
    expect(generatePixel({ pixel, event: eventInitialScript, campaignCode })).to.eql(
      '<!-- Facebook Pixel Code -->\n'
      + '<script>\n'
      + '!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n'
      + 'n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=\n'
      + "n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;\n"
      + 't.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\n'
      + "document,'script','https://connect.facebook.net/en_US/fbevents.js');\n"
      + `fbq('init', '${pixel}');\n`
      + `fbq('track', 'PageView', { campaign: '${campaignCode}' });\n`
      + '</script>\n'
      + '<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=123&ev=PageView&noscript=1"/></noscript>\n'
      + '<!-- DO NOT MODIFY -->\n'
      + '<!-- End Facebook Pixel Code -->\n',
    );
  });

  it('should render the correct script with additional tracking', () => {
    expect(generatePixel({ pixel, event: eventAdditionalTracking, campaignCode })).to.eql(
      '<script>\n'
      + `fbq('track', 'Purchase', { value: 0.00, currency: 'AUD', campaign: '${campaignCode}' });\n`
      + '</script>\n',
    );
  });
});
