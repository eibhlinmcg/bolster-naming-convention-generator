import { expect } from 'chai';
import generateString from '../../utils/generateString';
import nameTypes from '../../constants/nameTypes';


describe('generateString', () => {
  it('should generate the campaign name string', () => {
    expect(generateString(nameTypes.CAMPAIGN_NAME, {
      code: 'LNWY',
      campaign: 'Lanyway',
      tactic: 'Awareness',
      phase: 'PO',
    })).to.eql('[LNWY] Lanyway | t_Awareness | p_PO');
  });

  it('should generate the ad set name', () => {
    expect(generateString(nameTypes.AD_SET_NAME, {
      location: 'SYD',
      targetingType: 'LaL',
      targetingDetail: 'Alcohol Interests',
    })).to.eql('lo_SYD | LaL - Alcohol Interests');
  });

  it('should generate the ad name', () => {
    expect(generateString(nameTypes.AD_NAME, {
      adType: 'Canvas',
      assetType: 'Food Shot',
      creativeDetail: 'Sushi Shot',
    })).to.eql('Canvas | Food Shot - Sushi Shot');
  });
});
