import { expect } from 'chai';
import generateUtm from '../../utils/generateUtm';

const inputs = {
  url: 'https://goog.com',
  code: 'LNWY',
  medium: 'Email',
  source: 'fbig',
};

describe('Generate UTM', () => {
  it('should generate UTM based on inputs', () => {
    expect(generateUtm(inputs)).to.eql(
      'https://goog.com?utm_content=bolster&utm_campaign=LNWY&utm_medium=Email&utm_source=fbig',
    );
  });

  it('should generate UTM based on inputs with a custom source', () => {
    expect(generateUtm({ ...inputs, source_CUSTOM: 'anything' })).to.eql(
      'https://goog.com?utm_content=bolster&utm_campaign=LNWY&utm_medium=Email&utm_source=anything',
    );
  });

  it('should generate UTM based on inputs with a custom medium', () => {
    expect(generateUtm({ ...inputs, medium_CUSTOM: 'internet' })).to.eql(
      'https://goog.com?utm_content=bolster&utm_campaign=LNWY&utm_medium=internet&utm_source=fbig',
    );
  });

  it('should generate UTM based on inputs and not include medium if no medium was passed in params', () => {
    expect(generateUtm({ ...inputs, medium: undefined })).to.eql(
      'https://goog.com?utm_content=bolster&utm_campaign=LNWY&utm_source=fbig',
    );
  });
});
