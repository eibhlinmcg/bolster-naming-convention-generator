import events from '../popup/src/constants/pixel/events';

export default ({ pixel, event, campaignCode }) => {
  const { initialScript, additionalTracking, track } = events.find(({ name }) => name === event);
  return [
    initialScript ? '<!-- Facebook Pixel Code -->\n' : '',
    '<script>\n',
    initialScript
      ? '!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n'
      + 'n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=\n'
      + "n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;\n"
      + 't.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\n'
      + "document,'script','https://connect.facebook.net/en_US/fbevents.js');\n" : '',
    initialScript ? `fbq('init', '${pixel}');\n` : '',
    `fbq('track', '${track}', {${additionalTracking ? additionalTracking : ''} campaign: '${campaignCode}' });\n`,
    '</script>\n',
    initialScript
      ? '<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=123&ev=PageView&noscript=1"/></noscript>\n'
      + '<!-- DO NOT MODIFY -->\n'
      + '<!-- End Facebook Pixel Code -->\n' : '',
  ].join('');
};
