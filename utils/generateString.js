import nameTypes from '../constants/nameTypes';

const generateCampaignNameString = ({
  code, campaign, tactic, phase,
}) => (
  `[${code}] ${campaign} | t_${tactic} | p_${phase}`
);

const generateAdSetNameString = ({ location, targetingType, targetingDetail }) => (
  `lo_${location} | ${targetingType} - ${targetingDetail}`
);

const generateAdNameString = ({ adType, assetType, creativeDetail }) => (
  `${adType} | ${assetType} - ${creativeDetail}`
);

export default (nameType, inputValues) => {
  switch (nameType) {
    case nameTypes.CAMPAIGN_NAME: return generateCampaignNameString(inputValues);
    case nameTypes.AD_SET_NAME: return generateAdSetNameString(inputValues);
    case nameTypes.AD_NAME: return generateAdNameString(inputValues);
    default: return null;
  }
};
