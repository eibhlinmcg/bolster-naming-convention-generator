
export default ({
  url,
  code,
  medium,
  medium_CUSTOM: customMedium,
  source,
  source_CUSTOM: customSource,
}) => (
  [url,
    '?utm_content=bolster',
    '&utm_campaign=',
    code,
    (customMedium || (medium && medium !== '_CUSTOM') ? `&utm_medium=${customMedium || medium}` : ''),
    '&utm_source=',
    (customSource || source)].join('')
);
